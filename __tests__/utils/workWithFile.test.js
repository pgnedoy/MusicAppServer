describe('workWithFile tests', () => {
  afterEach(() => {
    jest.resetAllMocks();
    jest.resetModules();
  })
  it('mkdir returns resolving promise', () => {
    const mockMkdir = jest.fn((dest, mode, callback) => callback());
    jest.mock('fs', () => ({
      mkdir: mockMkdir
    }))
    const {
      mkdir
    } = require('../../src/utils/workWithFile');
    return mkdir('some/dir')
      .then(status => {
        expect(status).toEqual('ok');
        expect(mockMkdir.mock.calls.length).toEqual(1);
        expect(mockMkdir.mock.calls[0])
          .toEqual(expect.arrayContaining(['some/dir']))
      })
  })
  it('mkdir throws when an argument passed to callback', () => {
    const mockMkdir = jest.fn((dest, mode, callback) =>
      callback(new Error('Error during mkdir')))
    jest.mock('fs', () => ({
      mkdir: mockMkdir
    }))
    const {
      mkdir
    } = require('../../src/utils/workWithFile')
    expect.assertions(1)
    return mkdir('some/dir')
      .catch(e => expect(e.message).toEqual('Error during mkdir'))
  })
  it('check resolves with true when file or directory is present', () => {
    jest.mock('fs', () => ({
      lstat: jest.fn((path, callback) =>
        callback(undefined, {
          isDirectory: () => true,
          isFile: () => false
        }))
    }))
    const {
      check
    } = require('../../src/utils/workWithFile')
    return check().then(status =>
      expect(status).toEqual(true))
  })
  it('check resolves with false when non-existent file or dir error is present', () => {
    jest.mock('fs', () => ({
      lstat: jest.fn((path, callback) =>
        callback({
          stack: 'Error: ENOENT: no such file or directory, lstat \'/path/to/file\''
        }))
    }))
    const {
      check
    } = require('../../src/utils/workWithFile')
    return check('file').then(status =>
      expect(status).toEqual(false))
  })
  it('check rejects with other errors', () => {
    jest.mock('fs', () => ({
      lstat: jest.fn((path, callback) =>
        callback(new Error('Error: some error')))
    }))
    const {
      check
    } = require('../../src/utils/workWithFile')
    expect.assertions(1);
    return check('file').catch(err =>
      expect(err.message).toEqual('Error: some error'))
  })
});