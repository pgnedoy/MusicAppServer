describe('findMusic tests', () => {
    it('findAndSave resolves with correct object structure', () => {
        const mockSetKey = jest.fn();
        jest.mock('youtube-node', () => class {
            setKey() {
                return mockSetKey();
            }
            search(query, number, callback) {
                return callback(undefined, {
                    items: [{
                        id: {
                            videoId: 'ID1'
                        },
                        snippet: {
                            title: 'title1'
                        }
                    }, {
                        id: {
                            videoId: 'ID2'
                        },
                        snippet: {
                            title: 'title2'
                        }
                    }]
                })
            }
        })
        const { findMusic } = require('../../src/youTubeUtils/findMusic')
        return findMusic('query')
            .then(res => {
                expect(mockSetKey.mock.calls.length).toEqual(1);
                expect(res).toEqual([{
                    id: 'ID1',
                    name: 'title1'
                }, {
                    id: 'ID2',
                    name: 'title2'
                }])
            })
    })
});