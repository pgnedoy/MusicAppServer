describe('saveFile test', () => {
    it('expect saveFile to call console.log ', () => {
        jest.mock('fs', () => ({
            existsSync: () => false,
            mkdirSync: () => true,
            createWriteStream: dest => ({
                on: (actionName, callback) =>
                    callback()
            })
        })); 
        const mockPipe =  jest.fn();
        const mockStream = jest.fn(() => ({
            pipe: mockPipe
        }));
        const mockLog = jest.fn();
        global.console = {
            log: mockLog
        };
        jest.mock('youtube-audio-stream', ()=>mockStream);
        const saveAudioFile = require('../src/saveFile');
        return saveAudioFile('ID', 'FILE')
            .then(ok => {
                expect(ok).toEqual('./files/FILE.mp3');
                expect(mockLog.mock.calls.length).toEqual(1);
                expect(mockLog.mock.calls[0]).toEqual(['File download Completed', './files/FILE.mp3']);
                expect(mockStream.mock.calls.length).toEqual(1);
                expect(mockStream.mock.calls[0]).toEqual(['http://youtube.com/watch?v=ID']);
            })
    })
});