[![Build Status](https://travis-ci.org/DmitriySpirit/music-app-server.svg?branch=develop)](https://travis-ci.org/DmitriySpirit/music-app-server)
# music-app-server

1. Setup stages (Windows):
  - npm install --global --production windows-build-tools
  - Download https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-20180117-1b5d3c0-win64-static.zip, extract it and add inner `bin` folder to the PATH
  - npm install
  - Create in root empty folder `files`
  - npm start

2. Setup stages (Linux - Ubuntu 16.04):
  - sudo apt install ffmpeg -y
  - npm install
  - Create in root empty folder `files`
  - npm start
