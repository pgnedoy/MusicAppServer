#!/bin/bash

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update

sudo apt-get install -y mongodb-org
sudo systemctl start mongod
sudo systemctl status mongodb
sudo systemctl enable mongodb
# mongo
# use admin
# db.createUser(
#   {
#     user: "admin-root",
#     pwd: "1234567890",
#     roles: [ { role: "root", db: "admin" } ]
#   }
# )

# db.createUser(
#     {
#       user: "int20h",
#       pwd: "SomePassWord",
#       roles: [
#          { role: "readWrite", db: "test" }
#       ]
#     }
# )

# sudo nano /etc/mongod.conf
# sudo systemctl restart mongod
# sudo systemctl status mongod
# mongo -u admin -p --authenticationDatabase admin