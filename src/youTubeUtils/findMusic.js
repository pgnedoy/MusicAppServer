const YouTube = require('youtube-node')

const saveAudioFile = require('../saveFile')
const globalConfig = require('../config.json')

const youTube = new YouTube()
youTube.setKey(globalConfig.youTubeKey)

function findMusic(queryText, numberOfResults = 10) {
  return new Promise((resolve, reject) => {
    youTube.search(queryText, numberOfResults, (error, result) => {
      if (error) {
        console.log(error)
        reject(error)
      } else {
        resolve(result.items.map(item => ({
          id: item.id.videoId,
          name: item.snippet.title
        })))
      }
    })
  })
}

function findAndSaveFromYouTube(queryText) {
  return findMusic(queryText)
    .then((musicArray) => {
      return saveAudioFile(musicArray[0].id, musicArray[0].name)
    })
    .then((fileName) => {
      console.log(fileName, ' has been down loaded')
      return fileName
    })
}

module.exports = {
  findAndSaveFromYouTube,
  findMusic
}