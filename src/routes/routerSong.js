var express = require('express');
var router = express.Router();
var {Song} = require('../models/song');
var {Playlist}  = require('../models/playlist');

var User = require('../models/user');
var { sess } = require('./routerLogin');



var saveFileFromYouTube = require('../saveFile')


router.post('/delete', async function (req, res, next) {  
  // console.log("deleteSong", req.body)
  if (req.body.userId && req.body.playlist) {
    // console.log(req.body.playlist);
    await User.findById(req.body.userId, async function(error, user){
      if (error) {
        console.log("Error user");
      }else{
        await Playlist.findById(user.playlists[req.body.numPly], async function(error, playlist){
          if (error) {
            console.log("Error playlist");
          }else{
            await Playlist.findByIdAndUpdate(
              {_id: user.playlists[req.body.numPly]},
              { songs : req.body.playlist },
              { new: true }, 
              function(error, playlist){
                if (error) {
                  console.log("Adding - ERROR");
                } else {
                  console.log(playlist);
                  console.log("Song are deleted")
                }
              });
          }
        })
      }
    })
  }
    return res.json(req.body.playlist);
});

router.post('/findSongs', async function (req, res, next) {
    Song.find({'title' : { $regex: req.body.title, $options: 'i' }}, function(err, docs){
      console.log(docs || "ERROR!!!");
      res.json(docs);
    });
});

router.post('/songFromDB', async function (req, res, next) {
  // console.log("Hello", req.body);
  if (req.body.userId && req.body.songId && req.body.numPly != undefined) {
    await User.findById(req.body.userId, async function(error, user){
      if (error) {
        console.log("Error user");
      }else{
        await Playlist.findById(user.playlists[req.body.numPly], async function(error, playlist){
          if (error) {
            console.log("Error playlist");
          }else{
            // console.log('playlist', playlist)
            let add = playlist.songs;
            add[req.body.songId] = add.rootId;
            add.rootId = req.body.songId;
            await Playlist.findByIdAndUpdate(
              {_id: user.playlists[req.body.numPly]},
              { songs : add },
              { new: true }, 
              function(error, playlist){
                if (error) {
                  console.log("Adding - ERROR");
                } else {
                  // console.log(playlist);
                  console.log("Song are added")
                }
              });
          }
        })
      } 
    });   
  }
});

router.post('/', async function (req, res, next) {
  // console.log("song!!", req.body);
  let song;
  if (req.body.youtubeId && req.body.title && req.body.image && req.body.time) {
    const path = await saveFileFromYouTube(req.body.youtubeId, req.body.title)
    console.log("time", req.body.time)
    var songData = {
      path,
      youtubeId: req.body.youtubeId,
      title: req.body.title,
      image: req.body.image,
      time: req.body.time
    }
    song = await Song.create(songData);
    song.save() 
    // console.log("!!!!111")
    let currentUser = await User.findById(req.body.userId);
    await Playlist.findById(currentUser.playlists[req.body.numPly], async function(error, playlist){
      if (error) {
        console.log("Error playlist");
      }else{
        let add = playlist.songs;
        add[song._id] = add.rootId;
        add.rootId = song._id;
        console.log("Add", add)
        await Playlist.findByIdAndUpdate(
          {_id: currentUser.playlists[req.body.numPly]},
          { songs : add },
          { new: true }, 
          function(error, playlist){
            if (error) {
              console.log("Adding - ERROR");
            } else {
              // console.log(playlist);
              console.log("Song are added")
            }
          });
          return res.json(song)
      }
    })
  }
    
    
  })

module.exports = router;