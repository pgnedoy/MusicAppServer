var express = require('express');
var router = express.Router();
var {Playlist}  = require('../models/playlist');
var {Song} = require('../models/song');
var User = require('../models/user');


router.post('/', async function (req, res, next) {
  // console.log("hello");
  // console.log("body", req.body);
    if (req.body.userId )  {
      // let currentUser = 
      await User.findById(req.body.userId, async function(error, user){
        if(error){
          console.log("Users ERROR");
        }else{
          console.log("User Ok");
          await Playlist.findById(user.playlists[req.body.numPly], async function(error, playlist){
            if(error){
              console.log("Playlists ERROR");
            }else{
              console.log("1", playlist.songs.rootId );
              let songs = [];
              let songId = playlist.songs.rootId;
              // console.log(songId);
              while(songId){
                // console.log(item);
                // console.log("songId", songId);
                // let currentSong = \
                await Song.findById(songId, async function(error, song){
                  if(error){
                    console.log("Song  ERROR");
                  }else{
                    songs.push(song);
                    // console.log("!",playlist.songs[songId]);
                    songId = playlist.songs[songId]; 
                  }
                });
                // console.log("!!!!");       
              }

              // console.log("songs", songs);
              // await Playlist.findById(user.playlist[0], async function(error, playlist){
              // });
              return res.json({
                title:playlist.title,
                songs: songs    
              });
            }
          });
        }
      });
    }else{
      console.log("Get playlist on server ERROR");
    }
});

// router.post('/ply', async function (req, res, next) {
//   // console.log("hello");
//   console.log("body", req.body);
//     if (req.body.userId )  {
//       // let currentUser = 
//       await User.findById(req.body.userId, async function(error, user){
//         if(error){
//           console.log("Users ERROR");
//         }else{
//           let resp = [];
//           Promise.all(
//             user.playlists.map((ply) => Playlist.findById(ply))
//           ).then((plys) => {
//               console.log("plys", plys)
//               plys.map(playlist => {
//                 let songs = [];
//                 let songId = playlist.songs.rootId;
//                 while(songId){
//                   songs.push(songId);
//                   songId = playlist.songs[songId];
//                 }
//                 console.log("songs array", songs);
//                 let respSongs = [];
//                 Promise.all(
//                   songs.map(song => 
//                     Song.findById(song)
//                   )).then((songs) => {
//                     console.log("!!Songs",songs);
//                     resp.push({title: playlist.title, songs : songs })
//                     console.log("respSongs", JSON.stringify(resp));
//                   })
//                 // return respSongs;  
//               })
//               // console.log("resp",resp)
//               //
//               // return 
//             }).then(() => res.json({ songs : resp}))
//             }})
//           }else{
//              console.log("Get playlist on server ERROR");
//           }
        
// });

// \

router.get('/titles/:userId', async function (req, res, next) {
  console.log("111");
  
  if (req.params.userId) {
    // console.log("111", req.params.userId);
    await User.findById(req.params.userId, async function(error, user){
      if(error && !user){
        console.log("Titles!User ERROR!");
      }else{
        // console.log("2222");
        let titles = [];
        let playlists = user.playlists;
        // console.log("plailist", user.playlists);
        Promise.all(
          playlists.map((plyId, i) => 
            Playlist.findById(plyId)
        )
        ).then((playlists) =>{
          playlists.map((ply)=>{
            titles.push(ply.title);
          });
          console.log("333", titles);

          return res.json(titles);
        })
      }
    })
  }else{
    console.log("error");
  }

});



router.post('/checkChange', async function (req, res, next) {
  console.log("CheckChange", req.body);
  // console.log("req",req.body.list);
  let hashSongs = req.body.list;
    if (req.body.userId) {
      console.log('true');
      let currentUser = 
      await User.findById(req.body.userId, async function(error, user){
        if(error){
          console.log("CheckChange!User ERROR!");
        }else{
          await Playlist.findByIdAndUpdate(
            {_id: user.playlists[req.body.numPly]},
            { songs : req.body.list },
            { new: true }, 
            function(error, playlist){
              if (error) {
                console.log("eroro");
              } else {
                console.log(playlist);
                console.log("Playlist are upgrated")
              }
            });
        }
      });
  }  
});

router.post('/delete-playlist', async function (req, res, next) {
  console.log("Hello!!!!", req.body);
  if (req.body.userId && req.body.numPly != undefined) {
    await User.findById(req.body.userId, async function(error, user){
      if(error){
        console.log("DeletePlylist!User ERROR!");
      }else{
        let newPlaylists = [];
        for (let i = 0; i < user.playlists.length; i++) {
          if ( i != req.body.numPly){
            newPlaylists.push(user.playlists[i]);
          }
        }
        user.playlists = newPlaylists;
        await user.save();
        let titles = [];
        let playlists = user.playlists;
        // console.log("plailist", user.playlists);
        Promise.all(
          playlists.map((plyId, i) => 
            Playlist.findById(plyId)
        )
        ).then((playlists) =>{
          playlists.map((ply)=>{
            titles.push(ply.title);
          });
          console.log("333", titles);

          return res.json(titles);
        })
            
      }
      
        
  })}
})

router.post('/create', async function (req, res, next) {
    if (req.body.userId && req.body.title) {
      await User.findById(req.body.userId, async function(error, user){
        if(error){
          console.log("Titles!User ERROR!");
        }else{
          await Playlist.create( {songs: {rootId: ''}, title: req.body.title}, async function (err, ply) {
            if (err) {
              return handleError(err);
            }
            else{
              console.log("user", user);
              Promise.all([
                user.playlists.push(ply.id),
                user.save()]
              ).then(()=>{
                console.log("OK");
                return res.send("Ok");
              })
              
             
            } 
          })
        }
      })
    }else{
      console.log("!!!");
    }
  })


module.exports = router;



// "userId": "5af406f9044d931bb99e3349",
// "title": "ply1"