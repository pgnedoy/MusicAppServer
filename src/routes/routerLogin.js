var express = require('express');
var router = express.Router();
var User = require('../models/user');
var sess;
// var userId = '';
// GET route for reading data
// router.get('/', function (req, res, next) {
  // return res.sendFile(path.join(__dirname + '/templateLogReg/index.html'));
// });

//POST route for updating data

// router.delete('/', async function (req, res, next) {
//   const users = await Users.remove({});
//   return res.send('ok!');
// });

router.post('/', function (req, res, next) {
  console.log("!!!!!", req.body);
  // confirm that user typed same password twice
  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Passwords do not match.');
    err.status = 400;
    res.send("passwords dont match");
    return next(err);
  }
  if (req.body.userId) {
    User.findOneAndUpdate({_id: req.body.userId}, 
      { $set: { username: req.body.username }}, 
      { new: true }, 
      function(error, user) {
        if (error || !user) {
          var err = new Error('rewriting ERROR');
          err.status = 401;
          return next(err);
        } else {
          req.session.userId = user._id;
          // console.log(req.session.userId);
          return res.json(user);
        }

    }); 
  } else if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {
      console.log(" create user");
    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      passwordConf: req.body.passwordConf,
      imagepath: (req.body.imagePath)?"http://localhost:2020" + req.body.imagePath.slice(7) : "",
      playlist:[],
    }
    
      

    User.create(userData, function (error, user) {
      if (error) {
        return next(error);
      } else {
        // req.session.u  serId = user._id;
        User.authenticate(userData.email, userData.password, function (error, user) {
          if (error || !user) {
            var err = new Error('Wrong email or password.');
            err.status = 401;
            return next(err);
          } else {
        console.log("succes");
            
            return res.json(user);
          }
        });
      }
    });

  } else if (req.body.logemail && req.body.logpassword) {
    
    User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
      if (error || !user) {
    console.log("no!!!");
        
        
        
        var err = new Error('Wrong email or password.');
        err.status = 401;
        return next(err);
      } else {
        console.log("succes");
        req.session.userId = user._id;
        // userId = user._id;
        return res.json(user);
      }
    });
  } else {
    var err = new Error('All fields required.');
    err.status = 400;
    return next(err);
  }
});



router.post('/login', function (req, res, next) {
  if (req.body.logemail && req.body.logpassword) {
    User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
      if (error || !user) {
        console.log("no!!!");
        var err = new Error('Wrong email or password.');
        err.status = 401;
        return next(err);
      } else {
        console.log("succes");
        // req.session.userId = user._id;
        // userId = user._id;
        return res.json(user);
      }
    });
  }
});
// GET route after registering
router.get('/profile', function (req, res, next) {
  User.findById(req.session.userId)
    .exec(function (error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          var err = new Error('Not authorized! Go back!');
          err.status = 400;
          return next(err);
        } else {
          return res.send('<h1>Name: </h1>' + user.username + '<h2>Mail: </h2>' + user.email + '<br><a type="button" href="/logout">Logout</a>')
        }
      }
    });
});

// GET for logout logout
router.get('/logout', function (req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});
// exports.userId;
module.exports = { router, sess}
  // router:router,
  // sess:sess };