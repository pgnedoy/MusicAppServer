const fs = require('fs');
const stream = require('youtube-audio-stream');
const ytdl = require('ytdl-core')
const PassThrough = require('through2')

const globalConfig = require( './config.json');

module.exports = function saveAudioFile(videoId, fileName){
  return new Promise((resolve, reject) => {    
    if (!fs.existsSync(globalConfig.musicFolder)){      
      fs.mkdirSync(globalConfig.musicFolder);      
    }    
    const url = `http://youtube.com/watch?v=${videoId}`;  
    // conso  le.log(url);
      
    const dest = `${globalConfig.musicFolder}${fileName}.mp3`;    
    // console.log(dest);
    
    const file = fs.createWriteStream(dest);    
    // console.log(file);
    
    file.on('finish', function() {
      console.log("8");
      console.log("File download Completed", dest);
      resolve(dest);
    });
    const passThrough = new PassThrough();
    ytdl(url, {filter: 'audioonly', quality: 'lowest'}).pipe(passThrough).pipe(file);
    // stream(url).pipe(file);
    return dest;
  });
}