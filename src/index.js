const express = require('express')
var multer  = require('multer')
var upload = multer({ dest: 'public/uploads/' })
// var session = require('express-session'); // Сессии
// var MongoStore = require('connect-mongo')(session); // Хранилище сессий в монгодб
const path = require('path')
const bodyParser = require('body-parser')
const { findAndSaveFromYouTube } = require('./youTubeUtils/findMusic')
var cookieParser = require('cookie-parser')
const globalConfig = require('./config.json')

// TODO: delete logic of finding something in youtube.
// We need just api GET - /song/download/:id, that will save mp3 file and return it name.

// Needed API: 

//    GET    - /login ,
//    GET    - /logout ,

// {
//   id,
//   name,
//   email,
//   password,
//   playlists, 
// }
//    GET    - /user , - return array of all users
//    GET    - /user/:id ,
//    PUSH   - /user/:id ,
//    PUT    - /user/:id ,
//    DELETE - /user/:id ,

// {
//   id,
//   youTubeId, - add index on this field
//   fileName, - add index on this field
//   path,
//   rate,
//   time,
//   numberOfPlay
// }
//    GET    - /song , - return array of all songs
//    GET    - /song/:id ,
//    PUSH   - /song/:id ,
//    PUT    - /song/:id ,
//    DELETE - /song/:id ,

// {
//   id,
//   songs,
//   name,
// }
//    GET    - /playlist , - return array of all user playlists
//    GET    - /playlist/:id ,
//    PUSH   - /playlist/:id ,
//    PUT    - /playlist/:id ,
//    DELETE - /playlist/:id ,

// var mongoose = require('mongoose');

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
const app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://int20h:SomePassWord@localhost/test')

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('connected!')
});

app.use(cookieParser('work hard'))
app.use(session({
  secret: 'work hard',
  resave: false,
  saveUninitialized: false,
  // cookie: { maxAge: 6000000000   },
  
  store: new MongoStore({
    mongooseConnection: db
  }),
}));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})



app.use(function (req, res, next) {
  
  // console.log(res);
  // req.session = req.session;  
  // res.locals.session = req.session;
  // console.log('w')
  // if(res.loacals.session){
  //   console.log(res.loacals.session);
  // }
  // console.log("=======");
  // console.log(req.session);
  // console.log("=======");
  

  next()
})




app.use(`/${globalConfig.musicFuck}`, express.static(globalConfig.musicFuck))

app.get('/', (req, res) => {
  res.send('Hello World!')
})


var routesLogin = require('./routes/routerLogin');
app.use('/user', routesLogin.router);

var routesAvatar = require('./routes/routerAvatar');
app.use('/avatar', routesAvatar);

app.use(express.static('uploads'));

var routesSong = require('./routes/routerSong');
app.use('/song', routesSong);

var routesPlaylist = require('./routes/routerPlaylist');
app.use('/playlist', routesPlaylist);



// var routesPlaylist = require('./routes/routerPlaylist');
// app.use('/playlist', routesPlaylist);

app.get('/list', (req, res) => {
  const fs = require('fs')
  if (!fs.existsSync(globalConfig.musicFolder)){
    fs.mkdirSync(globalConfig.musicFolder);
  }
  fs.readdir(globalConfig.musicFolder, (err, files) => {
    res.send({
      list: files.map(fileName => ({
        name: fileName
      }))
    })
  })
})

app.get('/song/:songQuery', (req, res) => {
  console.log(req.params);
  findAndSaveFromYouTube(req.params.songQuery)
    .then(filePath => {
      console.log('===>', path.basename(filePath))
      res.send(path.basename(filePath))
    })
})



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});



app.listen(globalConfig.port, () =>
  console.log(`MusicApp server listening on port ${globalConfig.port}!`))