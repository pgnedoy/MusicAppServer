const fs = require('fs')

function mkdir(path, mode) {
  return new Promise((resolve, reject) => {
    fs.mkdir(path, mode, err => {
      if (err !== undefined)
        reject(err)
      else resolve('ok')
    })
  })
}

function check(path) {
  return new Promise((resolve, reject) => {
    fs.lstat(path, (err, stats) => {
      const template = `no such file or directory, lstat`;
      if (err !== undefined) {
        if (err.stack.includes(template) &&
          err.stack.includes(path)) resolve(false)
        else reject(err)
      }
      if (stats !== undefined &&
        (stats.isDirectory() || stats.isFile()))
        resolve(true)
      else reject(`Unknown stats format: ${stats}`)
    })
  })
}

module.exports = {
  mkdir,
  check
}