var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
// var { PlaylistShema } = require('./song')
var Schema = mongoose.Schema;


var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
  },
  passwordConf: {
    type: String,
    required: true,
  },
  imagepath:{
    type: String,
    required: false,
  },
  playlists: [{
      type:  Schema.Types.ObjectId,
      required: false
  }],
})

//authenticate input against database
UserSchema.statics.authenticate = function (email, password, callback) {
  User.findOne({ email: email })
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
        var err = new Error('User not found.');
        err.status = 401;
        return callback(err);
      }
      if (password == user.passwordConf) {
        return callback(null, user);        
      }else {
        return callback();
      }
      // bcrypt.compare(password, user.password, function (err, result) {
      //   if (result) {
      //     console.log("compare is success");
      //     return callback(null, user);
      //   } else {
      //     console.log("compare is no success");
          
      //     return callback();
      //   }
      // })
    });
}

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
  var user = this;
  bcrypt.hash(user.password, 10, function (err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  })
});


var User = mongoose.model('User', UserSchema);
module.exports = User;
