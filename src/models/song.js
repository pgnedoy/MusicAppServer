var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;


var SongShema = new mongoose.Schema({
  path: {
    type: String
  },
  youtubeId: {
    type: String
  },
  title: {
    type: String
  },
  image: {
    type: String
  },
  time: {
    type: String
  }

});


var Song = mongoose.model('Song', SongShema);
module.exports = {
  SongShema,
  Song,
};
