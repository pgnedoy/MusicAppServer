var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var PlaylistShema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  songs: Object
    // songId: {
    //   type: mongoose.Schema.Types.ObjectId
    // },
    // nextSongId:{
    //   type:  Schema.Types.ObjectId, ref: 'Song' 
    // }

});

var Playlist = mongoose.model('Playlist', PlaylistShema);

module.exports = {
  PlaylistShema,
  Playlist,
};